package com.gmail.slisenko.dm.getgif.buffer;

import com.gmail.slisenko.dm.getgif.dao.GifDao;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StopWatch;

import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.*;

@Slf4j
public class GifBuffer {
    private final GifDao dao;
    private final ConcurrentLinkedQueue<String> buffer;
    private final BufferWorker task;
    private final ExecutorService executor;
    private Future<?> status;

    public GifBuffer(GifDao dao) {
        this.dao = dao;
        this.buffer = new ConcurrentLinkedQueue<>();
        task = new BufferWorker();
        ThreadFactory factory = new ThreadFactoryBuilder()
                .setNameFormat("gif-loader")
                .setDaemon(true)
                .build();
        executor = Executors.newSingleThreadExecutor(factory);
    }

    public Optional<String> getGif() {
        Optional<String> gif = Optional.ofNullable(buffer.poll());
        if (!gif.isPresent()) {
            fillBuffer();
        }
        return gif;
    }

    private void fillBuffer() {
        if (status != null && !status.isDone()) {
            return;
        }
        status = executor.submit(task);
    }

    private class BufferWorker implements Runnable {
        @Override
        public void run() {
            StopWatch watch = new StopWatch();
            watch.start(String.valueOf(BufferWorker.class));
            log.info("Load GIFs to buffer...");
            try {
                buffer.addAll(dao.getGifs());
                watch.stop();
                log.info("GIFs uploaded in {} ms", watch.getLastTaskTimeMillis());
            } catch (IOException e) {
                log.error("Can't load GIFs: {}", e.getMessage());
            }
        }
    }
}
