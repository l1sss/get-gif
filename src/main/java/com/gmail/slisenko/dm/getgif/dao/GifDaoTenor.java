package com.gmail.slisenko.dm.getgif.dao;

import com.gmail.slisenko.dm.getgif.domain.Gif;
import com.gmail.slisenko.dm.getgif.domain.GifResponse;
import com.gmail.slisenko.dm.getgif.domain.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
public class GifDaoTenor implements GifDao {
    private final String randomGifUrl;
    private final RestTemplate rest;
    private String pos = "";

    public GifDaoTenor(String randomGifUrl, RestTemplate rest) {
        this.randomGifUrl = randomGifUrl;
        this.rest = rest;
    }

    @Override
    public List<String> getGifs() throws IOException {
        ResponseEntity<GifResponse> response = sendRequest();
        checkStatus(response);
        return parse(response);
    }

    private ResponseEntity<GifResponse> sendRequest() {
        return rest.getForEntity(randomGifUrl.concat(pos), GifResponse.class);
    }

    private void checkStatus(ResponseEntity<GifResponse> response) throws IOException {
        if (response == null) {
            throw new IOException("Null response from Tenor");
        }
        HttpStatus status = response.getStatusCode();
        if (!HttpStatus.OK.equals(status)) {
            throw new IOException("Bad response from Tenor, HTTP Status: " + status);
        }
    }

    private List<String> parse(ResponseEntity<GifResponse> response) throws IOException {
        List<String> gifs = new ArrayList<>();
        log.debug("Parse response...");
        GifResponse gifResponse = response.getBody();
        if (gifResponse == null) {
            throw new IOException("Null body from Tenor response");
        }
        Optional<List<Result>> results = Optional.ofNullable(gifResponse.getResults());
        results.ifPresent(resultList -> resultList.forEach(result -> result.getMedia().forEach(content -> {
            Gif gif = content.getGif();
            gifs.add(gif.getUrl());
        })));
        setNextPosition(gifResponse.getNext());
        return gifs;
    }

    private void setNextPosition(String next) {
        if (next != null && !next.isEmpty()) {
            pos = next;
        }
    }
}
