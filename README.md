# GET-GIF

### Repo
https://gitlab.com/l1sss/get-gif

### Description
A simple web application that returns random gif file

### Web
http://getgif-env.eba-xxziy7p7.us-east-2.elasticbeanstalk.com
