package com.gmail.slisenko.dm.getgif.service;

import com.gmail.slisenko.dm.getgif.buffer.GifBuffer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;

import java.util.Optional;

@Slf4j
public class GifService {
    private final String defaultGif;
    private final GifBuffer buffer;

    public GifService(@NonNull String defaultGif,
                      @NonNull GifBuffer buffer) {
        this.defaultGif = defaultGif;
        this.buffer = buffer;
    }

    public String getDefaultGif() {
        log.info("Get default GIF");
        return defaultGif;
    }

    public String getRandomGif() {
        log.info("Get random GIF");
        Optional<String> gif = buffer.getGif();
        return gif.orElseGet(this::getDefaultGif);
    }
}
